import java.util.LinkedList;

public class Set<E> {
	private LinkedList<E> list;
	
	public static void main(String[] args) {
		Set s = new Set(); // creates a new empty set
		System.out.println(s.isEmpty()); // makes sure that the set is empty right now
		s.add(5); // adds 5
		s.add(-4); // adds -4
		s.add(3); // adds 3
		s.add(5); // should NOT add 5 to the set because it already contains it
		System.out.println(s.contains(-4)); // should be true
		System.out.println(s.size()); // should print 3
		Set g = new Set(); // creates a new empty set
		g.add(7); // adds 7
		g.add(-3); // adds -3
		g.add(3); // adds 3
		Set k = s.union(g);
		k.display(); // should display 5, -4, 3, 7, -3
		Set r = s.intersection(g);
		r.display(); // should display 3
	}
	public Set() {
	list = new LinkedList<E>();	
	}
	
	public boolean isEmpty(){
		if(list.size() == 0)
			return true;
		else
			return false;			
	}
	
	public int size(){
		return list.size();
	}
	
	public void add(E item) {
	if(!list.contains(item))
		list.add(item);
	
	}
	
	public boolean contains(E item) {
		if(list.contains(item))
			return true;
		else
			return false;		
	}
	
	public Set<E> union(Set<E> input) {
		Set<E> k = new Set<E>();
		for(int i=0; i < list.size(); i++) {
			k.add(list.get(i));
		}
		for(int j=0; j < input.size(); j++) {
			if(!k.contains(input.get(j)))
				k.add(input.get(j));
		}
		
		return k;
	}
	
	public E get(int index) {
		return list.get(index);
	}
	
	public Set<E> intersection(Set<E> input) {
		Set<E> t = new Set<E>();
		for(int u=0; u < list.size(); u++) {
			if(input.contains(list.get(u)))
				t.add(list.get(u));
		}
		
		return t;	
	}
	
	public void removeAll() {
		list.clear();
	}
	
	public void display() {
		for(int p=0; p < list.size(); p++) {
		System.out.print(list.get(p) + ", ");
	}
		System.out.println();
}
}

